public class Team {
    Member membre;
    Member membre2;
    public Team(Member member, Member member2) {
        this.membre = member;
        this.membre2 = member2;
    }

    // La méthode main
    public static void main(String[] arg) {
        Member myMember = new Member("Auriel", "light",10,1);
        Member membre2 = new Member("Serge","joueur",15, 4);
        myMember.print();
        membre2.print();
        Team team1 = new Team(myMember, membre2);
        team1.membre.print();
        team1.membre2.print();
        Team team2 = new Team(membre2, myMember);
        team2.membre.print();
        team2.membre2.print();
    }
    static class Member {
        private String name;
        private String type;
        private int level;
        private int rank;

        public void print() {
            System.out.println(name +" "+  type +" "+ level +" "+ rank);
        }

        public Member(String name, String type, int level, int rank) {
            this.name = name;
            this.type = type;
            this.level = level;
            this.rank = rank;
            //définition des fonctions getters
        }
        public String getName() {
            return this.name;
        }
        public String getType() {
            return this.type;
        }
        public int getLevel() {
            return this.level;
        }
        public int getRank() {
            return this.rank;
        }
    }
}


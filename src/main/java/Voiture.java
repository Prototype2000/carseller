import java.lang.Math;
import java.util.Arrays;

/**
 * @author remit le plus beau
 */
public class Voiture {
    public static double randomBudget = 0;
    public static void main(String[] args) {
        String[] cars = {"Model S", "Model 3", "Cybertruck", "Model X", "Model Y"};
        String[] color = {"Rouge", "rose", "bleue", "violette", "verte", "orange", "grise", "jaune"};
        int[] price = {60000, 30000, 40000, 70000, 35000};
        // on définit le budget
        randomBudget = Math.floor(Math.random() * 200000);
        System.out.println(randomBudget);
        int min = min(price);
        int max = max(price);
        String[] achat = new String[1];
        while (randomBudget > 0) {
            // Utilisateur peut tout acheter, donc random l'achat
            if (randomBudget > max) {
                achat = addCarRandom(achat, cars, price);
                //détection de la plus chère
            } else if (randomBudget >= min) {
                achat = addMostExpensiveCar(achat, cars, price);
            } else {
                for(int k = 0; k < achat.length - 2; k++) {
                    System.out.println(achat[k]);
                }
                break;
            }
        }
    }
    /////////// Création des fonctions /////////////
    /**
     * ajoute voiture
     * @param arrayAchat c'est le tableau d'achat
     * @param arrayCar tableau de la liste des voitures
     * @param price tableau list des prix
     * @return tableau d'achat+1
     */
    public static String[] addCarRandom(String[] arrayAchat, String[] arrayCar, int[] price){
        double randomNumber = Math.floor(Math.random() * arrayCar.length);
        arrayAchat[arrayAchat.length - 1] = arrayCar[(int) randomNumber];
        randomBudget -= price[(int) randomNumber];
        return tailleBoard(arrayAchat);
    }

    public static String[] addMostExpensiveCar(String[] arrayAchat, String[] arrayCar, int[] price ) {
        Arrays.sort(price);
        Arrays.sort(arrayCar);
        for(int j = price.length - 1; j>= 0; j--) {
            if(randomBudget >= price[j]) {
                arrayAchat[arrayAchat.length - 1] = arrayCar[j];
                randomBudget -= price[j];
                arrayAchat = tailleBoard(arrayAchat);
                break;
            }
        }
        return tailleBoard(arrayAchat);
    }
    //augmentation du array de 1
    public static String[] tailleBoard(String[] board){
        String[] boardTempo = new String[board.length + 1];
        for(int l = 0; l < board.length; l++) {
            boardTempo[l] = board[l];
        }
        return boardTempo;
    }
    // Calcul du min
    public static int min(int[] price) {
        int[] priceTempo = Arrays.copyOf(price, price.length);
        Arrays.sort(priceTempo);
        return priceTempo[0];
    }
    public static int max(int[] price) {
        int[] priceTempo = Arrays.copyOf(price, price.length );
        Arrays.sort(priceTempo);
        return priceTempo[priceTempo.length - 1];
    }

}